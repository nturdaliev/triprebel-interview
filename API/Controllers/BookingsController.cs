﻿using BLL;
using DAL;
using DAL.Model;
using System;
using System.Linq;
using System.Web.Http;

namespace API.Controllers
{
    public class BookingsController : ApiController
    {
        private IHotelRepository HotelRepository;
        private IBookingManager BookingManager;

        public BookingsController() : this(DAL.HotelRepository.Instance, new BookingManager()) { }

        public BookingsController(IHotelRepository hotelRepository, IBookingManager bookingManager)
        {
            HotelRepository = hotelRepository;
            BookingManager = bookingManager;
        }

        public Booking Post(Booking booking)
        {
            var hotel = HotelRepository.GetHotels(booking.CheckIn, booking.CheckOut, booking.NumberOfGuests).SingleOrDefault(dbHotel => dbHotel.Id == booking.HotelId);
            var createdBooking = BookingManager.Create(hotel, booking.CheckIn, booking.CheckOut, booking.NumberOfGuests, booking.Coupon?.Code);

            return createdBooking;
        }
    }
}
