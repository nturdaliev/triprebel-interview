﻿using DAL;
using DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace API.Controllers
{
    public class HotelsController : ApiController
    {
        private IHotelRepository HotelRepository;

        public HotelsController() : this(DAL.HotelRepository.Instance) { }

        public HotelsController(IHotelRepository hotelRepository)
        {
            HotelRepository = hotelRepository;
        }
        
        public IEnumerable<Hotel> Get(DateTime checkIn, DateTime checkOut, int numberOfGuests)
        {
            return HotelRepository.GetHotels(checkIn, checkOut, numberOfGuests);
        }
        
        public Hotel Get(int id, DateTime checkIn, DateTime checkOut, int numberOfGuests)
        {
            return HotelRepository.GetHotels(checkIn, checkOut, numberOfGuests).SingleOrDefault(dbHotel => dbHotel.Id == id);
        }
    }
}
