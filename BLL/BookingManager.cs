﻿using DAL;
using DAL.Database;
using DAL.Model;
using System;
using System.Linq;

namespace BLL
{
    public class BookingManager : IBookingManager
    {
        private BookingContext BookingContext;
        private ICouponManager CouponManager;

        public BookingManager() : this(new BookingContext(), new CouponManager()) { }

        public BookingManager(BookingContext bookingContext, ICouponManager couponManager)
        {
            BookingContext = bookingContext;
            CouponManager = couponManager;
        }

        public Booking Create(Hotel hotel, DateTime checkIn, DateTime checkOut, int numberOfGuests, string couponCode = null)
        {
            var booking = new Booking()
            {
                Id = BookingContext.Bookings.Select(dbBooking => dbBooking.Id).DefaultIfEmpty(0).Max() + 1,
                HotelId = hotel.Id,
                CheckIn = checkIn,
                CheckOut = checkOut,
                NumberOfGuests = numberOfGuests,
                TotalRate = ((checkOut - checkIn).TotalDays * hotel.PricePerNight)
            };

            var coupon = BookingContext.Coupons.SingleOrDefault(dbCoupon => dbCoupon.Code == couponCode);

            CouponManager.Apply(booking, coupon);

            BookingContext.Bookings.Add(booking);
            BookingContext.SaveChanges();

            return booking;
        }
    }
}
