﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Model;

namespace BLL
{
    public class CouponManager : ICouponManager
    {
        public void Apply(Booking booking, Coupon coupon)
        {
            if(booking.TotalRate > coupon.ValidFrom && coupon.Value <= 100.0 && coupon.Value >= 0.0)
            {
                booking.Coupon = coupon;
                booking.TotalRate -= booking.TotalRate*coupon.Value/100;
            }
        }
    }
}
