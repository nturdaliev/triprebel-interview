﻿using DAL.Model;
using System;

namespace BLL
{
    public interface IBookingManager
    {
        Booking Create(Hotel hotel, DateTime checkIn, DateTime checkOut, int numberOfGuests, string couponCode = null);
    }
}