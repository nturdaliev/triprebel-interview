﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class Booking
    {
        public int Id { get; set; }
        public int HotelId { get; set; }
        public Coupon Coupon { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public int NumberOfGuests { get; set; }
        public double TotalRate { get; set; }
    }
}
