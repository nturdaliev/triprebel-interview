﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Model
{
    public class Coupon
    {
        public string Code { get; set; }
        public int Id { get; set; }
        public double ValidFrom { get; set; }
        public double Value { get; set; }
    }
}
