﻿using BLL;
using DAL.Database;
using DAL.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestClass]
    public class BookingTest
    {
        private Mock<BookingContext> context;
        private Mock<ICouponManager> couponManager;

        public BookingTest()
        {
            context = new Mock<BookingContext>();
            var data = new List<Booking>
            {
                new Booking()
                {
                    Id = 1,
                    CheckIn = DateTime.Now.AddDays(40),
                    CheckOut = DateTime.Now.AddDays(50)
                }
            }.AsQueryable();
            var mockSet = new Mock<DbSet<Booking>>();
            mockSet.As<IQueryable<Booking>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Booking>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Booking>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Booking>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            context.Setup(c => c.Bookings).Returns(mockSet.Object);

            var coupondata = new List<Coupon>
            {
                new Coupon()
                {
                    Id = 1,
                    Code = "TENBUCKS",
                    ValidFrom = 1000.0,
                    Value = 10.0
                }
            }.AsQueryable();
            var couponMockSet = new Mock<DbSet<Coupon>>();
            couponMockSet.As<IQueryable<Coupon>>().Setup(m => m.Provider).Returns(coupondata.Provider);
            couponMockSet.As<IQueryable<Coupon>>().Setup(m => m.Expression).Returns(coupondata.Expression);
            couponMockSet.As<IQueryable<Coupon>>().Setup(m => m.ElementType).Returns(coupondata.ElementType);
            couponMockSet.As<IQueryable<Coupon>>().Setup(m => m.GetEnumerator()).Returns(coupondata.GetEnumerator());
            context.Setup(c => c.Coupons).Returns(couponMockSet.Object);

            couponManager = new Mock<ICouponManager>();
            couponManager.Setup(m => m.Apply(It.IsAny<Booking>(), It.IsAny<Coupon>())).Verifiable();
        }

        [TestMethod]
        public void CalculatesPrice()
        {
            // Arrange
            var hotel = new Hotel()
            {
                Id = 1,
                Name = "The Hilton",
                PricePerNight = 2300.0
            };
            var manager = new BookingManager(context.Object, couponManager.Object);

            // Act
            var booking = manager.Create(hotel, DateTime.Now.AddDays(10), DateTime.Now.AddDays(12), 2);

            // Assert
            Assert.IsTrue(booking.HotelId == 1, "Copies the hotel ID");
            Assert.IsTrue(booking.TotalRate == 4600.0, "Price for two nights is twice the night price.");
        }

        [TestMethod]
        public void AppliesCoupon()
        {
            // Arrange
            var hotel = new Hotel()
            {
                Id = 1,
                Name = "The Hilton",
                PricePerNight = 2300.0
            };
            var manager = new BookingManager(context.Object, couponManager.Object);

            // Act
            var booking = manager.Create(hotel, DateTime.Now.AddDays(10), DateTime.Now.AddDays(12), 2, "TENBUCKS");

            // Assert
            couponManager.Verify(m => m.Apply(It.Is<Booking>(b => b.HotelId == 1), It.Is<Coupon>(c => c.Code == "TENBUCKS")), Times.Once);
        }


        [TestMethod]
        public void IgnoresInvalidCoupon()
        {
            // Arrange
            var hotel = new Hotel()
            {
                Id = 1,
                Name = "The Hilton",
                PricePerNight = 2300.0
            };
            var manager = new BookingManager(context.Object, couponManager.Object);

            // Act
            var booking = manager.Create(hotel, DateTime.Now.AddDays(10), DateTime.Now.AddDays(12), 2, "INVALIDCODE");

            // Assert
            couponManager.Verify(m => m.Apply(It.Is<Booking>(b => b.HotelId == 1), It.IsNotNull<Coupon>()), Times.Never);
        }
    }
}
