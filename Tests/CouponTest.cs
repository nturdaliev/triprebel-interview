﻿using BLL;
using DAL.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Tests
{
    [TestClass]
    public class CouponTest
    {
        [TestMethod]
        public void AcceptsValidCoupon()
        {
            var booking = new Booking()
            {
                Id = 1,
                CheckIn = DateTime.Now.AddDays(40),
                CheckOut = DateTime.Now.AddDays(50),
                HotelId = 1,
                NumberOfGuests = 2,
                TotalRate = 2000.0
            };

            var coupon = new Coupon()
            {
                Id = 1,
                Code = "SUPERDISCOUNT",
                ValidFrom = 1000.0,
                Value = 10.0
            };

            var manager = new CouponManager();
            manager.Apply(booking, coupon);

            Assert.IsTrue(booking.Coupon != null, "A coupon is applied");
            Assert.IsTrue(booking.TotalRate == 1800.0, "A discounted rate is calculated");
        }

        [TestMethod]
        public void RejectsInvalidCoupon()
        {
            var booking = new Booking()
            {
                Id = 1,
                CheckIn = DateTime.Now.AddDays(40),
                CheckOut = DateTime.Now.AddDays(45),
                HotelId = 1,
                NumberOfGuests = 2,
                TotalRate = 2000.0
            };

            var coupon = new Coupon()
            {
                Id = 1,
                Code = "SUPERDISCOUNT",
                ValidFrom = 3000.0,
                Value = 10.0
            };

            var manager = new CouponManager();
            manager.Apply(booking, coupon);

            Assert.IsTrue(booking.Coupon == null, "No coupon is applied");
            Assert.IsTrue(booking.TotalRate == 2000.0, "No discount is granted");
        }

        [TestMethod]
        public void RejectsInvalidCouponValue()
        {
            var booking = new Booking()
            {
                Id = 1,
                CheckIn = DateTime.Now.AddDays(40),
                CheckOut = DateTime.Now.AddDays(45),
                HotelId = 1,
                NumberOfGuests = 2,
                TotalRate = 2000.0
            };

            var coupon = new Coupon()
            {
                Id = 1,
                Code = "SUPERDISCOUNT",
                ValidFrom = 1000.0,
                Value = 101.0
            };

            var manager = new CouponManager();
            manager.Apply(booking, coupon);
            Assert.IsTrue(booking.Coupon == null, "No coupon is applied");
            Assert.IsTrue(booking.TotalRate == 2000.0, "No discount is granted");
        }

        [TestMethod]
        public void RejectsNegativeCouponValue()
        {
            var booking = new Booking()
            {
                Id = 1,
                CheckIn = DateTime.Now.AddDays(40),
                CheckOut = DateTime.Now.AddDays(45),
                HotelId = 1,
                NumberOfGuests = 2,
                TotalRate = 2000.0
            };

            var coupon = new Coupon()
            {
                Id = 1,
                Code = "SUPERDISCOUNT",
                ValidFrom = 1000.0,
                Value = -1.0
            };

            var manager = new CouponManager();
            manager.Apply(booking, coupon);
            Assert.IsTrue(booking.Coupon == null, "No coupon is applied");
            Assert.IsTrue(booking.TotalRate == 2000.0, "No discount is granted");
        }
    }
}
