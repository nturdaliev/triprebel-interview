﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using API.Controllers;
using DAL;
using Moq;
using DAL.Model;

namespace Tests
{
    [TestClass]
    public class HotelsControllerTest
    {
        [TestMethod]
        public void GetAll()
        {
            // Arrange
            var hotelRepository = new Mock<IHotelRepository>();
            hotelRepository.Setup(m => m.GetHotels(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<int>())).Returns(new List<Hotel>() {
                new Hotel()
                {
                    Id = 1,
                    Name = "The Hilton",
                    PricePerNight = 2300.0
                },

                new Hotel()
                {
                    Id = 2,
                    Name = "The Ritz",
                    PricePerNight = 1500.0
                }
            });
            var controller = new HotelsController(hotelRepository.Object);

            // Act
            var hotels = controller.Get(DateTime.Now.AddDays(10), DateTime.Now.AddDays(12), 2);

            // Assert
            Assert.IsTrue(hotels.Any(dbHotel => dbHotel.Id == 1), "Contains the Hilton");
            Assert.IsTrue(hotels.Any(dbHotel => dbHotel.Id == 2), "Contains the Ritz");
        }

        [TestMethod]
        public void GetSingle()
        {
            // Arrange
            var hotelRepository = new Mock<IHotelRepository>();
            hotelRepository.Setup(m => m.GetHotels(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<int>())).Returns(new List<Hotel>() {
                new Hotel()
                {
                    Id = 1,
                    Name = "The Hilton",
                    PricePerNight = 2300.0
                },

                new Hotel()
                {
                    Id = 2,
                    Name = "The Ritz",
                    PricePerNight = 1500.0
                }
            });
            var controller = new HotelsController(hotelRepository.Object);

            // Act
            var hotel1 = controller.Get(2, DateTime.Now.AddDays(10), DateTime.Now.AddDays(12), 2);
            var hotel2 = controller.Get(3, DateTime.Now.AddDays(10), DateTime.Now.AddDays(12), 2);

            // Assert
            Assert.IsTrue(hotel1.Id == 2, "Contains the Ritz");
            Assert.IsNull(hotel2, "Returns null for non-existant hotel");
        }
    }
}
